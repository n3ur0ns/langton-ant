import { writable, get } from 'svelte/store';

const DEFAULT_SIZE = 100;

const DEFAULT_ANT = {
    x: Math.trunc(DEFAULT_SIZE / 2),
    y: Math.trunc(DEFAULT_SIZE / 2),
    angle: 0,
    rotate: null,
    grid: new Map(),
    size: DEFAULT_SIZE,
    range: [...Array(DEFAULT_SIZE).keys()],
};

export const ant = (() => {
    const { subscribe, set } = writable(DEFAULT_ANT);
    return {
        subscribe,
        init: () => {
        },
        run: function () {
            let ant = process(get(this));

            // console.log(ant);

            set(ant);
        },
    };
})();

const move = (ant, angle) => {
    ant.rotate = angle + 'deg';

    ant.angle += angle;
    ant.angle %= 360;

    switch (ant.angle) {
        case 0:
            ant.y -= 1;
            break;
        case 90:
        case -270:
            ant.x += 1;
            break;
        case 180:
        case -180:
            ant.y += 1;
            break;
        case -90:
        case 270:
            ant.x -= 1;
            break;
    }

    return ant;
};

const whiteProcess = (ant, key) => {
    ant.grid.set(key, 0);
    let angle = 90;

    return move(ant, angle);
};

const blackProcess = (ant, key) => {
    ant.grid.set(key, 1);
    let angle = -90;

    return move(ant, angle);
};

const process = (ant) => {

    let key = `{x:${ant.x},y:${ant.y}}`;
    console.log(key);

    return !ant.grid.has(key)
        ? whiteProcess(ant, key)
        : ant.grid.get(key) === 0
            ? blackProcess(ant, key)
            : whiteProcess(ant, key);

};
